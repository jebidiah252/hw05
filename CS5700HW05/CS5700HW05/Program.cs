﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppLayer.PersonComponents;
using AppLayer.Matching;

namespace CS5700HW05
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MatchingPair> matches = new List<MatchingPair>();
            AdultToAdult matchingProgram = new AdultToAdult();
            matches = matchingProgram.SearchForMatches("PersonTestSet_11.xml");
            Console.WriteLine("Matches for PersonTestSet_11.xml");
            foreach(MatchingPair people in matches)
            {
                Console.WriteLine(people.ToString());
            }
            Console.WriteLine();
            matches.Clear();
            matches = matchingProgram.SearchForMatches("PersonTestSet_12.xml");
            Console.WriteLine("Matches for PersonTestSet_12.xml");
            foreach (MatchingPair people in matches)
            {
                Console.WriteLine(people.ToString());
            }
            Console.WriteLine();
            matches.Clear();
            matches = matchingProgram.SearchForMatches("PersonTestSet_13.xml");
            Console.WriteLine("Matches for PersonTestSet_13.xml");
            foreach (MatchingPair people in matches)
            {
                Console.WriteLine(people.ToString());
            }
            Console.WriteLine();
            matches.Clear();
            matches = matchingProgram.SearchForMatches("PersonTestSet_14.xml");
            Console.WriteLine("Matches for PersonTestSet_14.xml");
            foreach (MatchingPair people in matches)
            {
                Console.WriteLine(people.ToString());
            }
            Console.WriteLine();
            matches.Clear();
            matches = matchingProgram.SearchForMatches("PersonTestSet_15.xml");
            Console.WriteLine("Matches for PersonTestSet_15.xml");
            foreach (MatchingPair people in matches)
            {
                Console.WriteLine(people.ToString());
            }
            Console.ReadLine();
        }
    }
}
