﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CS5700HW05;
using AppLayer.IO;
using AppLayer.Matching;
using AppLayer.PersonComponents;
using System.Collections.Generic;

namespace CS5700HW05Testing
{
    [TestClass]
    public class InputCleanerTester
    {
        [TestMethod]
        public void CleanChildEmpty()
        {
            Child testing = new Child();
            InputCleaner clean = new InputCleaner();
            clean.cleanChild(ref testing);
            Assert.IsTrue(testing.BirthCounty == "" && testing.BirthDay == 0 && testing.BirthMonth == 0 && testing.BirthOrder == 0
                            && testing.BirthYear == 0 && testing.FirstName == "" && testing.Gender == ""
                            && testing.IsPartOfMultipleBirth == "" && testing.LastName == "" && testing.MiddleName == "" 
                            && testing.MotherFirstName == "" && testing.MotherLastName == "" && testing.MotherMiddleName == ""
                            && testing.NewbornScreeningNumber == "" && testing.ObjectId == 0 && testing.SocialSecurityNumber == ""
                            && testing.StateFileNumber == "");
        }

        [TestMethod]
        public void CleanAdultEmpty()
        {
            Adult testing = new Adult();
            InputCleaner clean = new InputCleaner();
            clean.cleanAdult(ref testing);
            Assert.IsTrue(testing.BirthDay == 0 && testing.BirthMonth == 0
                            && testing.BirthYear == 0 && testing.FirstName == "" && testing.Gender == ""
                            && testing.LastName == "" && testing.MiddleName == ""
                            && testing.ObjectId == 0 && testing.SocialSecurityNumber == ""
                            && testing.StateFileNumber == "" && testing.Phone1 == "" && testing.Phone2 == "");
        }

        [TestMethod]
        public void CleanChildFromData()
        {
            XMLImporterExporter importing = new XMLImporterExporter();
            List<Person> people = importing.Load("PersonTestSet_11.xml");
            Child testing = (Child)people[1];
            InputCleaner cleaner = new InputCleaner();
            cleaner.cleanChild(ref testing);
            Assert.IsTrue(testing.BirthCounty != "" || testing.BirthDay != 0 || testing.BirthMonth != 0 || testing.BirthOrder != 0
                            || testing.BirthYear != 0 || testing.FirstName != "" || testing.Gender != ""
                            || testing.IsPartOfMultipleBirth != "" || testing.LastName != "" || testing.MiddleName != ""
                            || testing.MotherFirstName != "" || testing.MotherLastName != "" || testing.MotherMiddleName != ""
                            || testing.NewbornScreeningNumber != "" || testing.ObjectId != 0 || testing.SocialSecurityNumber != ""
                            || testing.StateFileNumber != "");
        }

        [TestMethod]
        public void CleanAdultFromData()
        {
            XMLImporterExporter importing = new XMLImporterExporter();
            List<Person> people = importing.Load("PersonTestSet_11.xml");
            Adult testing = (Adult)people[0];
            InputCleaner cleaner = new InputCleaner();
            cleaner.cleanAdult(ref testing);
            Assert.IsTrue(testing.BirthDay != 0 || testing.BirthMonth != 0
                            || testing.BirthYear != 0 || testing.FirstName != "" || testing.Gender != ""
                            || testing.LastName != "" || testing.MiddleName != ""
                            || testing.ObjectId != 0 || testing.SocialSecurityNumber != ""
                            || testing.StateFileNumber != "" || testing.Phone1 != "" || testing.Phone2 != "");
        }
    }
}
