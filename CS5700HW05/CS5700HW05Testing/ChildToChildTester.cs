﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Matching;
using System.Collections.Generic;

namespace CS5700HW05Testing
{
    [TestClass]
    public class ChildToChildTester
    {
        // test methods 1 - 3 test the SearchForMatches method. They also test the LoadPeople method as well by using different filenames.
        [TestMethod]
        public void SearchForMatchesValidChToCh()
        {
            ChildToChild testing = new ChildToChild();
            List<MatchingPair> otherTesting = new List<MatchingPair>();
            otherTesting = testing.SearchForMatches("PersonTestSet_11.xml");
            Assert.IsNotNull(otherTesting.Count != 0);
        }

        [TestMethod]
        public void SearchForMatchesNoFilenameChToCh()
        {
            ChildToChild testing = new ChildToChild();
            List<MatchingPair> otherTesting = new List<MatchingPair>();
            otherTesting = testing.SearchForMatches("");
            Assert.IsNull(otherTesting);
        }

        [TestMethod]
        public void SearchForMatchesInvalidChToCh()
        {
            ChildToChild testing = new ChildToChild();
            List<MatchingPair> otherTesting = new List<MatchingPair>();
            otherTesting = testing.SearchForMatches("testData.txt");
            Assert.IsNull(otherTesting);
        }
    }
}
