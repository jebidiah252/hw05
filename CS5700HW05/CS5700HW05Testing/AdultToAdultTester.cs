﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Matching;
using System.Collections.Generic;

namespace CS5700HW05Testing
{
    [TestClass]
    public class AdultToAdultTester
    {
        // test methods 1 - 3 test the SearchForMatches method. They also test the LoadPeople method as well by using different filenames.
        [TestMethod]
        public void SearchForMatchesValidAdToAd()
        {
            AdultToAdult testing = new AdultToAdult();
            List<MatchingPair> otherTesting = new List<MatchingPair>();
            otherTesting = testing.SearchForMatches("PersonTestSet_11.xml");
            Assert.IsNotNull(otherTesting.Count != 0);
        }

        [TestMethod]
        public void SearchForMatchesNoFilenameAdToAd()
        {
            AdultToAdult testing = new AdultToAdult();
            List<MatchingPair> otherTesting = new List<MatchingPair>();
            otherTesting = testing.SearchForMatches("");
            Assert.IsNull(otherTesting);
        }

        [TestMethod]
        public void SearchForMatchesInvalidAdToAd()
        {
            AdultToAdult testing = new AdultToAdult();
            List<MatchingPair> otherTesting = new List<MatchingPair>();
            otherTesting = testing.SearchForMatches("testData.txt");
            Assert.IsNull(otherTesting);
        }
    }
}
