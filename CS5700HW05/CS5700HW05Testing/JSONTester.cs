﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CS5700HW05;
using AppLayer.IO;
using AppLayer.Matching;
using AppLayer.PersonComponents;
using System.Collections.Generic;

// DONE

namespace CS5700HW05Testing
{
    [TestClass]
    public class JSONTester
    {
        [TestMethod]
        public void JSONTestingNoErrors()
        {
            List<Person> people = new List<Person>();
            JSONImporterExporter testing = new JSONImporterExporter();
            people = testing.Load("PersonTestSet_11.json");
            Assert.IsNotNull(people);
        }

        [TestMethod]
        public void JSONTestingInvalidExtenstion()
        {
            List<Person> people = new List<Person>();
            JSONImporterExporter testing = new JSONImporterExporter();
            people = testing.Load("PersonTestSet_11.xml");
            Assert.IsNull(people);
        }

        [TestMethod]
        public void JSONTestingNoFilename()
        {
            List<Person> people = new List<Person>();
            JSONImporterExporter testing = new JSONImporterExporter();
            people = testing.Load("");
            Assert.IsNull(people);
        }

        [TestMethod]
        public void JSONTestingSavingValidFilename()
        {
            List<Person> people = new List<Person>();
            JSONImporterExporter testing = new JSONImporterExporter();
            people = testing.Load("PersonTestSet_11.json");
            testing.Save("testing.json", people);
            people.Clear();
            people = testing.Load("testing.json");
            Assert.IsNotNull(people);
        }

        [TestMethod]
        public void JSONTestingSavingInvalidFilename()
        {
            List<Person> people = new List<Person>();
            JSONImporterExporter testing = new JSONImporterExporter();
            people = testing.Load("PersonTestSet_11.json");
            testing.Save("testing.xml", people);
            people.Clear();
            people = testing.Load("testing.xml");
            Assert.IsNull(people);
        }

        [TestMethod]
        public void JSONTestingSavingNoFilename()
        {
            List<Person> people = new List<Person>();
            JSONImporterExporter testing = new JSONImporterExporter();
            people = testing.Load("PersonTestSet_11.json");
            testing.Save("", people);
            people.Clear();
            people = testing.Load("");
            Assert.IsNull(people);
        }
    }
}
