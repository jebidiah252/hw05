﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CS5700HW05;
using AppLayer.IO;
using AppLayer.Matching;
using AppLayer.PersonComponents;
using System.Collections.Generic;

// DONE

namespace CS5700HW05Testing
{
    [TestClass]
    public class XMLTester
    {
        [TestMethod]
        public void XMLTestingNoErrors()
        {
            List<Person> people = new List<Person>();
            XMLImporterExporter testing = new XMLImporterExporter();
            people = testing.Load("PersonTestSet_11.xml");
            Assert.IsNotNull(people);
        }

        [TestMethod]
        public void XMLTestingInvalidExtenstion()
        {
            List<Person> people = new List<Person>();
            XMLImporterExporter testing = new XMLImporterExporter();
            people = testing.Load("PersonTestSet_11.json");
            Assert.IsNull(people);
        }

        [TestMethod]
        public void XMLTestingNoFilename()
        {
            List<Person> people = new List<Person>();
            XMLImporterExporter testing = new XMLImporterExporter();
            people = testing.Load("");
            Assert.IsNull(people);
        }

        [TestMethod]
        public void XMLTestingSavingValidFilename()
        {
            List<Person> people = new List<Person>();
            XMLImporterExporter testing = new XMLImporterExporter();
            people = testing.Load("PersonTestSet_11.xml");
            testing.Save("testing.xml", people);
            people.Clear();
            people = testing.Load("testing.xml");
            Assert.IsNotNull(people);
        }

        [TestMethod]
        public void XMLTestingSavingInvalidFilename()
        {
            List<Person> people = new List<Person>();
            XMLImporterExporter testing = new XMLImporterExporter();
            people = testing.Load("PersonTestSet_11.xml");
            testing.Save("testing.json", people);
            people.Clear();
            people = testing.Load("testing.json");
            Assert.IsNull(people);
        }

        [TestMethod]
        public void XMLTestingSavingNoFilename()
        {
            List<Person> people = new List<Person>();
            XMLImporterExporter testing = new XMLImporterExporter();
            people = testing.Load("PersonTestSet_11.xml");
            testing.Save("", people);
            people.Clear();
            people = testing.Load("");
            Assert.IsNull(people);
        }
    }
}
