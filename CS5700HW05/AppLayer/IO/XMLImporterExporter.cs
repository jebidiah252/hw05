﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using AppLayer.PersonComponents;

namespace AppLayer.IO
{
    public class XMLImporterExporter : ImporterExporter
    {
        public override List<Person> Load(string filename)
        {
            List<Person> persons = null;
            XmlSerializer serializer = new XmlSerializer(typeof(List<Person>), new Type[] { typeof(Person), typeof(Child), typeof(Adult) });

            if (OpenReader(filename))
            {
                if(Path.GetExtension(filename) != ".xml")
                {
                    return persons;
                }
                try
                {
                    persons = serializer.Deserialize(Reader) as List<Person>;
                    int i = 0;
                    InputCleaner cleaner = new InputCleaner();
                    while (true)
                    {
                        if (persons[i].GetType() == typeof(Child))
                        {
                            Child temp = new Child();
                            temp = (Child)persons[i];
                            cleaner.cleanChild(ref temp);
                            persons.RemoveAt(i);
                            persons.Insert(i, temp);
                        }
                        else if (persons[i].GetType() == typeof(Adult))
                        {
                            Adult temp = new Adult();
                            temp = (Adult)persons[i];
                            cleaner.cleanAdult(ref temp);
                            persons.RemoveAt(i);
                            persons.Insert(i, temp);
                        }
                        i++;
                        if (persons.Count == i)
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.Write(e);
                }
                Reader.Close();
            }

            return persons;
        }

        public override void Save(string filename, List<Person> persons)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Person>), new Type[] { typeof(Person), typeof(Child), typeof(Adult) });

            if (OpenWriter(filename))
            {
                if (filename == "")
                {
                    return;
                }
                serializer.Serialize(Writer, persons);
                Writer.Close();
            }
        }
    }
}
