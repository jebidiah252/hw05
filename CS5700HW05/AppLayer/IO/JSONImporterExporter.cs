﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using AppLayer.PersonComponents;

namespace AppLayer.IO
{
    public class JSONImporterExporter : ImporterExporter
    {
        public override List<Person> Load(string filename)
        {
            List<Person> persons = null;
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<Person>), new Type[] { typeof(Person), typeof(Adult), typeof(Child) });
            StreamReader bla = new StreamReader(filename);
            string line;
            while((line = bla.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
            if (OpenReader(filename))
            {
                try
                {
                    persons = (List<Person>)serializer.ReadObject(Reader.BaseStream);

                    int i = 0;
                    InputCleaner cleaner = new InputCleaner();
                    while (true)
                    {
                        if (persons[i].GetType() == typeof(Child))
                        {
                            Child temp = new Child();
                            temp = (Child)persons[i];
                            cleaner.cleanChild(ref temp);
                            persons.RemoveAt(i);
                            persons.Insert(i, temp);
                        }
                        else if (persons[i].GetType() == typeof(Adult))
                        {
                            Adult temp = new Adult();
                            temp = (Adult)persons[i];
                            cleaner.cleanAdult(ref temp);
                            persons.RemoveAt(i);
                            persons.Insert(i, temp);
                        }
                        i++;
                        if (persons.Count == i)
                            break;
                    }

                    Reader.Close();
                }
                catch (Exception e)
                {
                    Console.Write(e);
                }
            }

            return persons;
        }

        public override void Save(string filename, List<Person> persons)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<Person>), new Type[] { typeof(Person), typeof(Adult), typeof(Child) });

            if (OpenWriter(filename))
            {
                serializer.WriteObject(Writer.BaseStream, persons);
                Writer.Close();
            }
        }
    }
}
