﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppLayer.PersonComponents;

namespace AppLayer.Matching
{
    public class ChildToAdult : MatchingAlgorithm
    {
        protected override bool LookForMatches(Person personOne, Person personTwo)
        {
            if (personOne.GetType() != typeof(Child) && personTwo.GetType() != typeof(Adult))
            {
                Console.WriteLine("Invalid persons passed into the LookForMatches method");
                return false;
            }
            for(int i = 0; i < 5; i++)
            {
                switch(i)
                {
                    case 0:
                        if (((personOne.StateFileNumber != "" && personTwo.StateFileNumber != "")
                        || (personOne.SocialSecurityNumber != "" && personTwo.SocialSecurityNumber != ""))
                        && ((personOne.BirthYear != 0 && personTwo.BirthYear != 0)
                        && (personOne.BirthMonth != 0 && personTwo.BirthMonth != 0)
                        && (personOne.BirthDay != 0 && personTwo.BirthDay != 0)))
                        {
                            if ((personOne.BirthDay == personTwo.BirthDay
                            && personOne.BirthMonth == personTwo.BirthMonth
                            && personOne.BirthYear == personTwo.BirthYear)
                            && ((personOne.StateFileNumber == personTwo.StateFileNumber)
                            || (personOne.SocialSecurityNumber == personTwo.SocialSecurityNumber)))
                            {
                                return true;
                            }
                        }
                        break;
                    case 1:
                        if (((personOne.StateFileNumber != "" && personTwo.StateFileNumber != "")
                        || (personOne.SocialSecurityNumber != "" && personTwo.SocialSecurityNumber != ""))
                        && (((personOne.FirstName != null && personTwo.FirstName != null) && (personOne.MiddleName != null && personTwo.MiddleName != null))
                        || ((personOne.MiddleName != null && personTwo.MiddleName != null) && (personOne.LastName != null && personTwo.LastName != null))
                        || ((personOne.LastName != null && personTwo.LastName != null) && (personOne.FirstName != null && personTwo.FirstName != null))))
                        {
                            if (((personOne.StateFileNumber == personTwo.StateFileNumber)
                            || (personOne.SocialSecurityNumber == personTwo.SocialSecurityNumber))
                            && ((personOne.FirstName == personTwo.FirstName && personOne.MiddleName == personTwo.MiddleName)
                            || (personOne.FirstName == personTwo.FirstName && personOne.LastName == personTwo.LastName)
                            || (personOne.MiddleName == personTwo.MiddleName && personOne.LastName == personTwo.LastName)))
                            {
                                return true;
                            }

                        }
                        break;
                    case 2:
                        if (((personOne.FirstName != "" && personTwo.FirstName != "")
                        && (personOne.MiddleName != "" && personTwo.MiddleName != "")
                        && (personOne.LastName != "" && personTwo.LastName != ""))
                        && (personOne.Gender != "" && personTwo.Gender != "")
                        && ((personOne.BirthDay != 0 && personTwo.BirthDay != 0)
                        && (personOne.BirthMonth != 0 && personTwo.BirthMonth != 0)
                        && (personOne.BirthYear != 0 && personTwo.BirthYear != 0)))
                        {
                            if (((personOne.FirstName == personTwo.FirstName)
                            && (personOne.MiddleName == personTwo.MiddleName)
                            && (personOne.LastName == personTwo.LastName))
                            && (personOne.Gender == personTwo.Gender)
                            && ((personOne.BirthDay == personTwo.BirthDay)
                            && (personOne.BirthMonth == personTwo.BirthMonth)
                            && (personOne.BirthYear == personTwo.BirthYear)))
                            {
                                return true;
                            }
                        }
                        break;
                    case 3:
                        if (((personOne.BirthDay != 0 && personOne.BirthDay != 0)
                        && (personOne.BirthMonth != 0 && personOne.BirthMonth != 0)
                        && (personOne.BirthYear != 0 && personOne.BirthYear != 0))
                        && (personOne.SocialSecurityNumber != "" && personOne.SocialSecurityNumber != ""))
                        {
                            if (((personOne.FirstName == personTwo.FirstName)
                            && (personOne.MiddleName == personTwo.MiddleName)
                            && (personOne.LastName == personTwo.MiddleName))
                            && (personOne.SocialSecurityNumber == personTwo.SocialSecurityNumber))
                            {
                                return true;
                            }
                        }
                        break;
                            default:
                        if (personOne == personTwo)
                            return true;
                        break;
                }
            }
            return false;
        }
    }
}
