﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Matching
{
    public class MatchingPair
    {
        protected int firstObjectID;

        protected int secondObjectID;

        public MatchingPair(int firstObjID, int secondObjID)
        {
            firstObjectID = firstObjID;
            secondObjectID = secondObjID;
        }

        public override string ToString()
        {
            return "Person " + firstObjectID + " and Person " + secondObjectID + " are a match.";
        }
    }
}
