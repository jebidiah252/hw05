﻿using System;
using System.Collections.Generic;
using System.IO;
using AppLayer.PersonComponents;
using AppLayer.IO;

namespace AppLayer.Matching
{
    public abstract class MatchingAlgorithm
    {
        protected ArrayOfPerson persons;

        public MatchingAlgorithm()
        {
            persons = new ArrayOfPerson();
        }
        public List<MatchingPair> SearchForMatches(string filename)
        {
            List<MatchingPair> matchingPairs = null;
            if(LoadPeople(filename))
                matchingPairs = StoreMatches(persons);
            return matchingPairs;
        }

        private bool LoadPeople(string filename)
        {
            string extension = Path.GetExtension(filename);
            if (extension == ".xml")
            {
                persons.storage = new XMLImporterExporter();
            }
            else if (extension == ".json")
            {
                persons.storage = new JSONImporterExporter();
            }
            else
            {
                Console.WriteLine("You are using a file type that this program does not support.");
                persons = null;
                return false;
            }

            persons.Load(filename);
            return true;
        }
        protected abstract bool LookForMatches(Person personOne, Person personTwo);
        protected List<MatchingPair> StoreMatches(ArrayOfPerson persons)
        {
            List<MatchingPair> matches = new List<MatchingPair>();
            foreach (var person in persons)
            {
                foreach (var otherPerson in persons)
                {
                    if(person.GetType() == typeof(Child) && otherPerson.GetType() == typeof(Adult))
                    {
                        var matching = new ChildToAdult();
                        if (matching.LookForMatches(person, otherPerson) && person.ObjectId != otherPerson.ObjectId)
                        {
                            matches.Add(new MatchingPair(person.ObjectId, otherPerson.ObjectId));
                        }
                    }
                    else if(person.GetType() == typeof(Adult) && otherPerson.GetType() == typeof(Adult))
                    {
                        var matching = new AdultToAdult();
                        if (matching.LookForMatches(person, otherPerson) && person.ObjectId != otherPerson.ObjectId)
                        {
                            matches.Add(new MatchingPair(person.ObjectId, otherPerson.ObjectId));
                        }
                    }
                    else if(person.GetType() == typeof(Child) && otherPerson.GetType() == typeof(Child))
                    {
                        var matching = new ChildToChild();
                        if (matching.LookForMatches(person, otherPerson) && person.ObjectId != otherPerson.ObjectId)
                        {
                            matches.Add(new MatchingPair(person.ObjectId, otherPerson.ObjectId));
                        }
                    }
                }
            }
            return matches;
        }
    }
}
